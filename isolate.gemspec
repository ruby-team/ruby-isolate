#########################################################
# This file has been automatically generated by gem2tgz #
#########################################################
# -*- encoding: utf-8 -*-
# stub: isolate 3.3.1 ruby lib

Gem::Specification.new do |s|
  s.name = "isolate".freeze
  s.version = "3.3.1"

  s.required_rubygems_version = Gem::Requirement.new(">= 1.8.2".freeze) if s.respond_to? :required_rubygems_version=
  s.require_paths = ["lib".freeze]
  s.authors = ["Ryan Davis".freeze, "Eric Hodel".freeze, "John Barnette".freeze]
  s.cert_chain = ["-----BEGIN CERTIFICATE-----\nMIIDPjCCAiagAwIBAgIBAjANBgkqhkiG9w0BAQUFADBFMRMwEQYDVQQDDApyeWFu\nZC1ydWJ5MRkwFwYKCZImiZPyLGQBGRYJemVuc3BpZGVyMRMwEQYKCZImiZPyLGQB\nGRYDY29tMB4XDTE0MDkxNzIzMDcwN1oXDTE1MDkxNzIzMDcwN1owRTETMBEGA1UE\nAwwKcnlhbmQtcnVieTEZMBcGCgmSJomT8ixkARkWCXplbnNwaWRlcjETMBEGCgmS\nJomT8ixkARkWA2NvbTCCASIwDQYJKoZIhvcNAQEBBQADggEPADCCAQoCggEBALda\nb9DCgK+627gPJkB6XfjZ1itoOQvpqH1EXScSaba9/S2VF22VYQbXU1xQXL/WzCkx\ntaCPaLmfYIaFcHHCSY4hYDJijRQkLxPeB3xbOfzfLoBDbjvx5JxgJxUjmGa7xhcT\noOvjtt5P8+GSK9zLzxQP0gVLS/D0FmoE44XuDr3iQkVS2ujU5zZL84mMNqNB1znh\nGiadM9GHRaDiaxuX0cIUBj19T01mVE2iymf9I6bEsiayK/n6QujtyCbTWsAS9Rqt\nqhtV7HJxNKuPj/JFH0D2cswvzznE/a5FOYO68g+YCuFi5L8wZuuM8zzdwjrWHqSV\ngBEfoTEGr7Zii72cx+sCAwEAAaM5MDcwCQYDVR0TBAIwADALBgNVHQ8EBAMCBLAw\nHQYDVR0OBBYEFEfFe9md/r/tj/Wmwpy+MI8d9k/hMA0GCSqGSIb3DQEBBQUAA4IB\nAQAFoDJRokCQdxFfOrmsKX41KOFlU/zjrbDVM9hgB/Ur999M6OXGSi8FitXNtMwY\nFVjsiAPeU7HaWVVcZkj6IhINelTkXsxgGz/qCzjHy3iUMuZWw36cS0fiWJ5rvH+e\nhD7uXxJSFuyf1riDGI1aeWbQ74WMwvNstOxLUMiV5a1fzBhlxPqb537ubDjq/M/h\nzPUFPVYeL5KjDHLCqI2FwIk2sEMOQgjpXHzl+3NlD2LUgUhHDMevmgVua0e2GT1B\nxJcC6UN6NHMOVMyAXsr2HR0gRRx4ofN1LoP2KhXzSr8UMvQYlwPmE0N5GQv1b5AO\nVpzF30vNaJK6ZT7xlIsIlwmH\n-----END CERTIFICATE-----\n".freeze]
  s.date = "2015-02-02"
  s.description = "Isolate is a very simple RubyGems sandbox. It provides a way to\nexpress and automatically install your project's Gem dependencies.".freeze
  s.email = ["ryand-ruby@zenspider.com".freeze, "drbrain@segment7.net".freeze, "code@jbarnette.com".freeze]
  s.extra_rdoc_files = ["CHANGELOG.rdoc".freeze, "Manifest.txt".freeze, "README.rdoc".freeze]
  s.files = [".autotest".freeze, ".gemtest".freeze, "CHANGELOG.rdoc".freeze, "Manifest.txt".freeze, "README.rdoc".freeze, "Rakefile".freeze, "lib/hoe/isolate.rb".freeze, "lib/isolate.rb".freeze, "lib/isolate/completely.rb".freeze, "lib/isolate/entry.rb".freeze, "lib/isolate/events.rb".freeze, "lib/isolate/now.rb".freeze, "lib/isolate/rake.rb".freeze, "lib/isolate/sandbox.rb".freeze, "test/fixtures/blort-0.0.gem".freeze, "test/fixtures/isolate.rb".freeze, "test/fixtures/override.rb".freeze, "test/fixtures/override.rb.local".freeze, "test/fixtures/system/specifications/rcov-0.9.9.gemspec".freeze, "test/fixtures/system_redundant/specifications/rake-0.8.7.gemspec".freeze, "test/fixtures/system_redundant/specifications/rcov-0.9.9.gemspec".freeze, "test/fixtures/with-hoe/specifications/hoe-2.3.3.gemspec".freeze, "test/fixtures/with-hoe/specifications/rake-0.8.7.gemspec".freeze, "test/fixtures/with-hoe/specifications/rubyforge-1.0.4.gemspec".freeze, "test/isolate/test.rb".freeze, "test/test_isolate.rb".freeze, "test/test_isolate_entry.rb".freeze, "test/test_isolate_events.rb".freeze, "test/test_isolate_sandbox.rb".freeze]
  s.homepage = "http://github.com/jbarnette/isolate".freeze
  s.licenses = ["MIT".freeze]
  s.rdoc_options = ["--main".freeze, "README.rdoc".freeze]
  s.rubygems_version = "2.7.6".freeze
  s.summary = "Isolate is a very simple RubyGems sandbox".freeze
  s.test_files = ["test/test_isolate.rb".freeze, "test/test_isolate_entry.rb".freeze, "test/test_isolate_events.rb".freeze, "test/test_isolate_sandbox.rb".freeze]

  if s.respond_to? :specification_version then
    s.specification_version = 4

    if Gem::Version.new(Gem::VERSION) >= Gem::Version.new('1.2.0') then
      s.add_development_dependency(%q<ZenTest>.freeze, ["~> 4.5"])
      s.add_development_dependency(%q<hoe>.freeze, ["~> 3.13"])
      s.add_development_dependency(%q<hoe-doofus>.freeze, ["~> 1.0"])
      s.add_development_dependency(%q<hoe-git>.freeze, ["~> 1.3"])
      s.add_development_dependency(%q<hoe-seattlerb>.freeze, ["~> 1.2"])
      s.add_development_dependency(%q<minitest>.freeze, ["~> 5.5"])
      s.add_development_dependency(%q<rdoc>.freeze, ["~> 4.0"])
    else
      s.add_dependency(%q<ZenTest>.freeze, ["~> 4.5"])
      s.add_dependency(%q<hoe>.freeze, ["~> 3.13"])
      s.add_dependency(%q<hoe-doofus>.freeze, ["~> 1.0"])
      s.add_dependency(%q<hoe-git>.freeze, ["~> 1.3"])
      s.add_dependency(%q<hoe-seattlerb>.freeze, ["~> 1.2"])
      s.add_dependency(%q<minitest>.freeze, ["~> 5.5"])
      s.add_dependency(%q<rdoc>.freeze, ["~> 4.0"])
    end
  else
    s.add_dependency(%q<ZenTest>.freeze, ["~> 4.5"])
    s.add_dependency(%q<hoe>.freeze, ["~> 3.13"])
    s.add_dependency(%q<hoe-doofus>.freeze, ["~> 1.0"])
    s.add_dependency(%q<hoe-git>.freeze, ["~> 1.3"])
    s.add_dependency(%q<hoe-seattlerb>.freeze, ["~> 1.2"])
    s.add_dependency(%q<minitest>.freeze, ["~> 5.5"])
    s.add_dependency(%q<rdoc>.freeze, ["~> 4.0"])
  end
end
